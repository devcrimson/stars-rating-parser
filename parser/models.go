package parser

import "gitlab.com/devcrimson/stars-rating-parser/amoapi"

type Entry struct {
	Rank       int    `db:"rank"`
	Name       string `db:"name"`
	Username   string `db:"username"`
	Country    string `db:"country"`
	Topics     string `db:"topics"`
	Followers  string `db:"followers"`
	Engagement string `db:"engagement"`
}

func (e *Entry) ToContact() *amoapi.ContactToAdd {
	return &amoapi.ContactToAdd{
		Name: e.Name,
		Fields: []*amoapi.CustomField{
			{Name: "username", Value: e.Username},
			{Name: "country", Value: e.Country},
			{Name: "topics", Value: e.Topics},
			{Name: "followers", Value: e.Followers},
			{Name: "engagement", Value: e.Engagement},
			{Name: "alive", Value: "1"},
		},
	}
}
