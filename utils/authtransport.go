package utils

import (
	"net/http"
)

type authTransport struct {
	auth  string
	inner http.RoundTripper
}

func (t *authTransport) RoundTrip(r *http.Request) (*http.Response, error) {
	r.Header.Add("Authorization", t.auth)
	return t.inner.RoundTrip(r)
}

func NewAuthTransport(auth string, inner http.RoundTripper) http.RoundTripper {
	return &authTransport{auth, inner}
}
