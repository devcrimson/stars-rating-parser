package utils

import (
	"fmt"
	"io"
	"net/http"
)

type HttpError struct {
	Code int
	Body []byte
}

func (h *HttpError) Error() string {
	return fmt.Sprintf(
		"Request failed with code: %d\nBody: %s",
		h.Code,
		string(h.Body),
	)
}

func ConsumeResponse(r *http.Response) ([]byte, error) {
	body, err := io.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	if r.StatusCode == 200 {
		return body, nil
	}
	return nil, &HttpError{
		r.StatusCode,
		body,
	}
}
