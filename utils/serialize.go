package utils

import "strings"

type ToJSON interface {
	ToJSON() string
}

func SerializeArray[T ToJSON](items []T) string {
	if len(items) == 0 {
		return "[]"

	}
	builder := strings.Builder{}
	builder.WriteByte('[')
	builder.WriteString(items[0].ToJSON())
	for _, v := range items[1:] {
		builder.WriteByte(',')
		builder.WriteString(v.ToJSON())
	}
	builder.WriteByte(']')
	return builder.String()
}
