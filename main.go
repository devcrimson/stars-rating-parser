package main

import (
	_ "github.com/lib/pq"
	"gitlab.com/devcrimson/stars-rating-parser/amoapi"
	"gitlab.com/devcrimson/stars-rating-parser/oauth"
	"gitlab.com/devcrimson/stars-rating-parser/parser"
	"os"
	"time"
)

func main() {
	dur, err := time.ParseDuration(os.Getenv("REFRESH_DURATION"))
	perr(err)
	entries, err := parser.ParseFromSite(os.Getenv("PARSER_COUNTRY"))
	perr(err)
	auth, err := oauth.RequestForAuth()
	perr(err)
	tokens, err := oauth.RetrieveTokens(auth)
	perr(err)
	api := amoapi.NewAPI(auth.Referer, tokens)
	_ = api.CreateAdditionalFields()
	_, err = api.AddContacts(toContactSlice(entries))
	perr(err)
	for {
		time.Sleep(dur)
		entries, err = parser.ParseFromSite(os.Getenv("PARSER_COUNTRY"))
		perr(err)
		contacts, err := api.GetContacts()
		perr(err)
		var newContacts []*amoapi.ContactToAdd
		var updatedContacts []*amoapi.ContactToUpdate
		for _, v := range toContactSlice(entries) {
			if contact := getContactWithName(v.Name, contacts); contact != nil {
				updatedContacts = append(updatedContacts, &amoapi.ContactToUpdate{
					ID:     contact.ID,
					Fields: v.Fields,
				})
			} else {
				newContacts = append(newContacts, &amoapi.ContactToAdd{
					Name:   v.Name,
					Fields: v.Fields,
				})
			}
		}
		var deletedContacts []*amoapi.ContactToUpdate
		for _, v := range contacts {
			if !existsInEntries(v.Name, entries) {
				deletedContacts = append(deletedContacts, &amoapi.ContactToUpdate{
					ID: v.ID,
					Fields: []*amoapi.CustomField{{
						Name:  "alive",
						Value: "0",
					}},
				})
			}
		}
		_, err = api.AddContacts(newContacts)
		perr(err)
		perr(api.UpdateContacts(append(updatedContacts, deletedContacts...)))
	}
}

type toContact interface {
	ToContact() *amoapi.ContactToAdd
}

func toContactSlice[T toContact](entries []T) []*amoapi.ContactToAdd {
	r := make([]*amoapi.ContactToAdd, 0, len(entries))
	for _, v := range entries {
		r = append(r, v.ToContact())
	}
	return r
}

func getContactWithName(name string, contacts []*amoapi.ContactsContact) *amoapi.ContactsContact {
	for _, v := range contacts {
		if v.Name == name {
			return v
		}
	}
	return nil
}

func existsInEntries(name string, entries []*parser.Entry) bool {
	for _, v := range entries {
		if v.Name == name {
			return true
		}
	}
	return false
}

func perr(err error) {
	if err != nil {
		panic(err)
	}
}
