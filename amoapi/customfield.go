package amoapi

import (
	"fmt"
	"strings"
)

type CustomField struct {
	Name  string
	Value string
}

func (c *CustomField) ToJSON() string {
	const template = `{"field_name":"%s","field_code":"%s","values":[{"value":"%s"}]}`
	return fmt.Sprintf(template, c.Name, strings.ToUpper(c.Name), c.Value)
}
