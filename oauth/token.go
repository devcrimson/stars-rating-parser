package oauth

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
)

type TokensResult struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
	ExpiresIn    int    `json:"expires_in"`
}

func RetrieveTokens(r *AuthResult) (*TokensResult, error) {
	const template = `{"client_id":"%s","client_secret":"%s","grant_type":"authorization_code","code":"%s","redirect_uri":"%s"}`
	resp, err := http.Post(
		fmt.Sprintf("https://%s/oauth2/access_token", r.Referer),
		"application/json",
		strings.NewReader(fmt.Sprintf(
			template,
			os.Getenv("OAUTH_CLIENT_ID"),
			os.Getenv("OAUTH_CLIENT_SECRET"),
			r.Code,
			os.Getenv("OAUTH_REDIRECT_URI"),
		)),
	)
	if err != nil {
		return nil, err
	}
	b := resp.Body
	defer func() { _ = b.Close() }()
	bytes, err := io.ReadAll(b)
	if err != nil {
		return nil, err
	}
	// Не вижу смысла создавать структуру для одного внутреннего поля
	data := new(TokensResult)
	return data, json.Unmarshal(bytes, &data)
}
