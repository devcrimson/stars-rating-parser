package oauth

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"os"
)

const AuthTemplate = "https://www.amocrm.ru/oauth?client_id=%s&mode=popup"

type AuthResult struct {
	Referer string
	Code    string
}

func RequestForAuth() (*AuthResult, error) {
	err := openBrowser(fmt.Sprintf(AuthTemplate, os.Getenv("OAUTH_CLIENT_ID")))
	if err != nil {
		return nil, err
	}
	ctx, cancel := context.WithCancel(context.Background())
	onSuccess := make(chan *AuthResult)
	onError := make(chan error)
	go func() {
		server := http.Server{
			Addr: os.Getenv("OAUTH_SERVER_ADDR"),
			Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				q := r.URL.Query()
				qCode := q.Get("code")
				if qCode != "" {
					writeFlush(w, "Success. You can close this page.")
					onSuccess <- &AuthResult{
						Referer: q.Get("referer"),
						Code:    qCode,
					}
				} else {
					writeFlush(w, "Access denied by user. You can close this page.")
					onError <- fmt.Errorf("oAuth failed: %s", q.Get("error"))
				}
				cancel()
			}),
			BaseContext: func(net.Listener) context.Context { return ctx },
		}
		err = server.ListenAndServe()
		if err != nil {
			onError <- err
		}
	}()
	select {
	case s := <-onSuccess:
		return s, nil
	case e := <-onError:
		return nil, e
	}
}

func writeFlush(w http.ResponseWriter, content string) {
	_, _ = w.Write([]byte(content))
	w.(http.Flusher).Flush()
}
